package com.examples.demo.demoapp;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

public class NewsPojo {

    @SerializedName("status")
    public String status;

    @SerializedName("totalResults")
    public String totalResults;

    @SerializedName("articles")
    public List<ArticalsPojo> arrArticle = null;


    public class ArticalsPojo {

        @SerializedName("source")
        public Source source;

        @SerializedName("author")
        public String author="";

        @SerializedName("title")
        public String title="";

        @SerializedName("description")
        public String description="";

        @SerializedName("url")
        public String url="";

        @SerializedName("urlToImage")
        public String urlToImage="";

        @SerializedName("publishedAt")
        public String publishedAt="";

        class Source {

            @SerializedName("id")
            public String id="";

            @SerializedName("name")
            public String name="";

        }

    }
}
