package com.examples.demo.demoapp;

import android.support.design.widget.Snackbar;
import android.view.View;

public class MyUtils {

    public static void showSnackBarDoubleInfo(View view, String markerSet, final String clickOk) {
        Snackbar snackbar = Snackbar
                .make(view, markerSet, Snackbar.LENGTH_LONG)
                .setAction("Got it", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(view, clickOk, Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                    }
                });

        snackbar.show();
    }

    public static void showSnackBarDefault(View view, String latLng) {
        Snackbar snackbar = Snackbar.make(view, latLng, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
