package com.examples.demo.demoapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


class MyNewsAdapter extends RecyclerView.Adapter<MyNewsAdapter.ViewHolder>{

    List<NewsPojo.ArticalsPojo> arrArticle;
    Context mContext;
    MyNewsAdapter(Context mContext, List<NewsPojo.ArticalsPojo> arrArticle) {
        this.arrArticle=arrArticle;
        this.mContext=mContext;
    }

    @Override
    public MyNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, null);

        // create ViewHolder

        MyNewsAdapter.ViewHolder viewHolder = new MyNewsAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.tvNewsTittle.setText(arrArticle.get(position).source.name);
        Picasso.with(mContext).load(arrArticle.get(position).urlToImage).into(viewHolder.imgIcon);
        viewHolder.tvTittle.setText(arrArticle.get(position).title);
        viewHolder.tvDescription.setText(arrArticle.get(position).description);
        viewHolder.tvPublishedBy.setText("~ Published On : "+arrArticle.get(position).publishedAt);
        viewHolder.tvAuthorName.setText("by "+arrArticle.get(position).author);
    }

    @Override
    public int getItemCount() {
        return arrArticle.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvNewsTittle,tvTittle,tvDescription,tvPublishedBy,tvAuthorName;
        ImageView imgIcon;

        ViewHolder(View view) {
            super(view);
            imgIcon = view.findViewById(R.id.imgIcon);
            tvNewsTittle = view.findViewById(R.id.tvNewsTittle);
            tvTittle = view.findViewById(R.id.tvTittle);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvPublishedBy = view.findViewById(R.id.tvPublishedBy);
            tvAuthorName = view.findViewById(R.id.tvAuthorName);
        }
    }

}
