package com.examples.demo.demoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        findViewById(R.id.tvMaps).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToActivity(MapsActivity.class);
            }
        });

        findViewById(R.id.tvNews).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToActivity(NewsActivity.class);
            }
        });
    }

    /**
     * This method is used to navigate to selected activity.
     * @param aclass class name
     */
    private void navigateToActivity(Class aclass) {
        startActivity(new Intent(this, aclass));
    }
}
