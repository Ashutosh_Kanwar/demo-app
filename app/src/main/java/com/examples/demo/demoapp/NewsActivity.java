package com.examples.demo.demoapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsActivity extends AppCompatActivity {

    private APIInterface apiInterface;
    private RecyclerView recyclerView;
    private ProgressDialog progress;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        recyclerView = findViewById(R.id.recyclerView);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        callService();
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading news...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }

    /**
     * This method is used to call service using retrofit.
     */
    private void callService() {
        Call<NewsPojo> call = apiInterface.doGetListResources();
        call.enqueue(new Callback<NewsPojo>() {
            @Override
            public void onResponse(Call<NewsPojo> call, Response<NewsPojo> response) {
                Log.d("TAG2", "" + response.body().arrArticle.get(0));
                progress.dismiss();
                recyclerView.setLayoutManager(new LinearLayoutManager(NewsActivity.this));
                MyNewsAdapter mAdapter = new MyNewsAdapter(NewsActivity.this,response.body().arrArticle);
                recyclerView.setAdapter(mAdapter);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            }

            @Override
            public void onFailure(Call<NewsPojo> call, Throwable t) {
                call.cancel();
            }
        });

    }
}
