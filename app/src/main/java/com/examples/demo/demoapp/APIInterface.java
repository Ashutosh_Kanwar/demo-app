package com.examples.demo.demoapp;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {
    @GET("top-headlines?country=in&category=science&apiKey=1cb38a557539471db9707463a2df66bb")
    Call<NewsPojo> doGetListResources();

}
